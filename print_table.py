# Declare a table and print it
I = 3 # Number of rows
J = 4 # Number of columns

# Initialize the table
# There's got to be a way that doesn't involve literal declaration.
# But I don't know what that better way is.
table = [range(J)]
#print table[0]
for i in range(I-1):
    table.append(range(J))
#    for j in range(J):
#        print "i:" + str(i) + " j:" + str(j) + " "
#    print "\n"

print "Table 0"
for row in table:
    PRINT_STRING = ''
    print row
    for col in row:
        print col
        PRINT_STRING = PRINT_STRING + str(col) + "\t"
    print PRINT_STRING + "\n"


print "Table 1"
i = 0
for row in table:
    ROW_VALUES = []
    j = 0
    for col in row:
        ROW_VALUES.append("Tag " + str(i) + ". Column Data " + str(j))
        j = j+1
    row = ROW_VALUES
    table[i] = row
    print row
    i = i+1

print "Table 1"
for row in table:
    print row

print "Table 1"
for row in table:
    PRINT_STRING = ''
    for col in row:
        PRINT_STRING = PRINT_STRING + str(col) + "\t"
    print PRINT_STRING + "\n"
