#file: execution_order.py
#author: Ben Gramkow
#description: demonstrates what happens to code placed before function defintions.
print("This code executes before main.")

def functionA():
    print("Function A")

def functionB():
    print("Function B")

def functionC():
    print("Function C")

def main():
    functionA()
    functionB()
    functionC()

# __name__ contains the calling function, or it contains
# '__main__' if this is executing as the main function
if __name__ == '__main__':
    main()
